package com.panyok.exchangerates;

import android.app.Application;
import android.content.Context;

import com.panyok.exchangerates.dagger.component.AppComponent;
import com.panyok.exchangerates.dagger.component.DaggerAppComponent;
import com.panyok.exchangerates.dagger.module.AppModule;




public class ExchangeRatesApplication extends Application {


    private AppComponent appComponent;


    public static ExchangeRatesApplication get(Context context) {
        return (ExchangeRatesApplication) context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
    }


    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }


    public AppComponent getAppComponent() {
        return appComponent;
    }
}
