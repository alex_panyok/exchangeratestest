package com.panyok.exchangerates.dagger.component;

import com.panyok.exchangerates.dagger.module.ApiModule;
import com.panyok.exchangerates.dagger.module.AppModule;
import com.panyok.exchangerates.dagger.module.CurrencyListModule;
import com.panyok.exchangerates.dagger.module.ExchangeRateModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent  {

    CurrencyListComponent plus(CurrencyListModule module);

    ExchangeRateComponent plus(ExchangeRateModule module);

}
