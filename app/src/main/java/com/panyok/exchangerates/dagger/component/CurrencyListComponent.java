package com.panyok.exchangerates.dagger.component;

import com.panyok.exchangerates.dagger.module.CurrencyListModule;
import com.panyok.exchangerates.dagger.scope.ActivityScope;
import com.panyok.exchangerates.mvp.activity.CurrencyListActivity;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = CurrencyListModule.class)
public interface CurrencyListComponent extends BaseComponent {

    CurrencyListActivity inject(CurrencyListActivity activity);


}
