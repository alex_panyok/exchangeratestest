package com.panyok.exchangerates.dagger.component;



public interface DaggerElement<T extends BaseComponent> {

    T getComponent();
}
