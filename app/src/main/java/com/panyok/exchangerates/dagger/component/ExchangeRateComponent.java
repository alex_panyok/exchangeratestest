package com.panyok.exchangerates.dagger.component;

import com.panyok.exchangerates.dagger.module.ExchangeRateFragmentModule;
import com.panyok.exchangerates.dagger.module.ExchangeRateModule;
import com.panyok.exchangerates.dagger.scope.ActivityScope;
import com.panyok.exchangerates.mvp.activity.ExchangeRatesActivity;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = ExchangeRateModule.class)
public interface ExchangeRateComponent extends BaseComponent {

    ExchangeRatesActivity inject(ExchangeRatesActivity activity);

    ExchangeRateFragmentComponent plus(ExchangeRateFragmentModule module);

}
