package com.panyok.exchangerates.dagger.component;

import android.support.v4.app.Fragment;

import com.panyok.exchangerates.dagger.module.ExchangeRateFragmentModule;
import com.panyok.exchangerates.dagger.scope.FragmentScope;
import com.panyok.exchangerates.mvp.fragment.ExchangeRateFragment;

import dagger.Subcomponent;

/**
 * Created by Dell on 10.05.2017.
 */
@FragmentScope
@Subcomponent(modules = ExchangeRateFragmentModule.class)
public interface ExchangeRateFragmentComponent{

    ExchangeRateFragment inject(ExchangeRateFragment fragment);
}
