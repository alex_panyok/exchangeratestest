package com.panyok.exchangerates.dagger.module;

import android.app.Application;
import android.content.Context;

import com.panyok.exchangerates.BuildConfig;
import com.panyok.exchangerates.R;
import com.panyok.exchangerates.network.ApiService;
import com.panyok.exchangerates.network.ApiWorker;
import com.panyok.exchangerates.network.RxErrorHandlingCallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



@Module
public class ApiModule {


    private static final int CACHE_SIZE =  10_000_000;

    private static final int HTTP_TIMEOUT = 60_000;

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Context context) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }
        builder.cache(new Cache(context.getExternalCacheDir(), CACHE_SIZE));
        builder.connectTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS);

        return builder.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRestAdapter(Application application, OkHttpClient okHttpClient) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(okHttpClient)
                .baseUrl(application.getString(R.string.endpoint))
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());
        return builder.build();
    }


    @Provides
    @Singleton
    public ApiWorker provideApiWorker(ApiService service){
        return new ApiWorker(service);
    }



    @Provides
    @Singleton
    public ApiService provideApiService(Retrofit restAdapter) {
        return restAdapter.create(ApiService.class);
    }


}
