package com.panyok.exchangerates.dagger.module;

import android.app.Application;
import android.content.Context;

import com.panyok.exchangerates.mvp.presenter.PresenterCache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class AppModule {

    private Application application;

    public AppModule(Application application){ this.application = application;}

    @Provides
    @Singleton
    public Application provideApplication() {
        return application;
    }


    @Provides
    @Singleton
    public PresenterCache providesPresenterCache(){return new PresenterCache();}


    @Provides
    @Singleton
    public Context provideContext() {
        return application.getApplicationContext();
    }

}
