package com.panyok.exchangerates.dagger.module;

import com.panyok.exchangerates.dagger.scope.ActivityScope;
import com.panyok.exchangerates.util.AdapterKeys;
import com.panyok.exchangerates.util.ExchangeRatesKeys;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;


@Module
public class BaseActivityModule implements AdapterKeys, ExchangeRatesKeys {

    @ActivityScope
    @Provides
    @Named(ACT_SUBSCRIPTION)
    CompositeSubscription providesSubscription(){
        return new CompositeSubscription();
    }
}
