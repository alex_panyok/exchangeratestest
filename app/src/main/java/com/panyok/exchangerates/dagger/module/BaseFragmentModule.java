package com.panyok.exchangerates.dagger.module;

import com.panyok.exchangerates.dagger.scope.FragmentScope;
import com.panyok.exchangerates.mvp.activity.BaseActivity;
import com.panyok.exchangerates.mvp.fragment.BaseFragment;
import com.panyok.exchangerates.util.AdapterKeys;
import com.panyok.exchangerates.util.ExchangeRatesKeys;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;


@Module
public abstract class BaseFragmentModule<F extends BaseFragment> implements ExchangeRatesKeys, AdapterKeys {

    protected F fragment;

    @FragmentScope
    @Provides
    BaseActivity providesBaseActivity(){
        return (BaseActivity) fragment.getActivity();
    }


    @FragmentScope
    @Provides
    @Named(FRAG_SUBSCRIPTION)
    CompositeSubscription providesFragmentSubscription(){
        return new CompositeSubscription();
    }

}
