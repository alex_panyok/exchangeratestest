package com.panyok.exchangerates.dagger.module;

import android.support.v7.widget.LinearLayoutManager;

import com.panyok.exchangerates.dagger.scope.ActivityScope;
import com.panyok.exchangerates.mvp.activity.CurrencyListActivity;
import com.panyok.exchangerates.mvp.adapter.CurrencyListAdapter;
import com.panyok.exchangerates.mvp.adapter.RVHolderFactory;
import com.panyok.exchangerates.mvp.adapter.holder.ItemCurrencyHolderFactory;
import com.panyok.exchangerates.mvp.interactor.CurrencyListActivityInteractor;
import com.panyok.exchangerates.mvp.interactor.impl.CurrencyListActivityInteractorImpl;
import com.panyok.exchangerates.mvp.presenter.CurrencyListActivityPresenter;
import com.panyok.exchangerates.mvp.presenter.impl.CurrencyListActivityPresenterImpl;
import com.panyok.exchangerates.mvp.view.CurrencyListActivityView;
import com.panyok.exchangerates.network.ApiWorker;

import java.util.Map;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;
import rx.subscriptions.CompositeSubscription;


@Module
public class CurrencyListModule extends BaseActivityModule {

    private CurrencyListActivity activity;


    public CurrencyListModule(CurrencyListActivity activity) {
        this.activity = activity;
    }

    @ActivityScope
    @Provides
    CurrencyListActivity providesActivity() {
        return activity;
    }

    @ActivityScope
    @Provides
    CurrencyListActivityView providesCurrencyListActivityView() {
        return activity;
    }

    @ActivityScope
    @Provides
    CurrencyListActivityInteractor providesCurrencyListActivityInteractor(ApiWorker apiWorker, @Named(ACT_SUBSCRIPTION) CompositeSubscription subscription) {
        return new CurrencyListActivityInteractorImpl(apiWorker, subscription);
    }

    @ActivityScope
    @Provides
    CurrencyListActivityPresenter providesCurrencyListActivityPresenter(CurrencyListActivityInteractor interactor) {
        return new CurrencyListActivityPresenterImpl(interactor);
    }

    @ActivityScope
    @Provides
    LinearLayoutManager providesLayoutManager(CurrencyListActivity context) {
        return new LinearLayoutManager(context);
    }

    @ActivityScope
    @Provides
    CurrencyListAdapter providesCurrencyListAdapter(CurrencyListActivity context, Map<Integer, RVHolderFactory> map) {
        return new CurrencyListAdapter(context, map);
    }

    @Provides
    @IntoMap
    @IntKey(ITEM_CURRENCY)
    RVHolderFactory provideViewHolderCategory() {
        return new ItemCurrencyHolderFactory();
    }


}
