package com.panyok.exchangerates.dagger.module;

import android.support.v7.widget.LinearLayoutManager;

import com.panyok.exchangerates.dagger.scope.FragmentScope;
import com.panyok.exchangerates.mvp.activity.BaseActivity;
import com.panyok.exchangerates.mvp.adapter.RVHolderFactory;
import com.panyok.exchangerates.mvp.adapter.RatesListAdapter;
import com.panyok.exchangerates.mvp.adapter.holder.ItemCurrencyHolderFactory;
import com.panyok.exchangerates.mvp.fragment.ExchangeRateFragment;
import com.panyok.exchangerates.mvp.interactor.ExchangeRateFragmentInteractor;
import com.panyok.exchangerates.mvp.interactor.impl.ExchangeRateFragmentInteractorImpl;
import com.panyok.exchangerates.mvp.presenter.ExchangeRateFragmentPresenter;
import com.panyok.exchangerates.mvp.presenter.impl.ExchangeRateFragmentPresenterImpl;
import com.panyok.exchangerates.mvp.view.ExchangeRateFragmentView;
import com.panyok.exchangerates.network.ApiWorker;

import java.util.Map;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;
import rx.subscriptions.CompositeSubscription;


@Module
public class ExchangeRateFragmentModule extends BaseFragmentModule<ExchangeRateFragment> {


    public ExchangeRateFragmentModule(ExchangeRateFragment fragment) {
        this.fragment = fragment;
    }

    @FragmentScope
    @Provides
    ExchangeRateFragmentView providesExchangeRateFragmentView(){
        return fragment;
    }



    @FragmentScope
    @Provides
    ExchangeRateFragmentInteractor providesExchangeRateFragmentInteractor(ApiWorker apiWorker, @Named(FRAG_SUBSCRIPTION) CompositeSubscription subscription) {
        return new ExchangeRateFragmentInteractorImpl(apiWorker, subscription);
    }

    @FragmentScope
    @Provides
    ExchangeRateFragmentPresenter providesExchangeRateFragmentPresenter(ExchangeRateFragmentInteractor interactor) {
        return new ExchangeRateFragmentPresenterImpl(interactor);
    }


    @FragmentScope
    @Provides
    RatesListAdapter providesRatesListAdapter(BaseActivity context, Map<Integer, RVHolderFactory> map) {
        return new RatesListAdapter(context, map);
    }

    @FragmentScope
    @Provides
    LinearLayoutManager providesLinearLayoutManager(BaseActivity context){
        return new LinearLayoutManager(context);
    }

    @Provides
    @IntoMap
    @IntKey(ITEM_CURRENCY)
    RVHolderFactory provideViewHolderCategory() {
        return new ItemCurrencyHolderFactory();
    }

}
