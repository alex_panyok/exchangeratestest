package com.panyok.exchangerates.dagger.module;

import com.panyok.exchangerates.dagger.scope.ActivityScope;
import com.panyok.exchangerates.mvp.activity.ExchangeRatesActivity;
import com.panyok.exchangerates.mvp.adapter.FragmentAdapterCurrency;

import dagger.Module;
import dagger.Provides;


@Module
public class ExchangeRateModule extends BaseActivityModule {

    private ExchangeRatesActivity activity;


    public ExchangeRateModule(ExchangeRatesActivity activity) {
        this.activity = activity;
    }

    @ActivityScope
    @Provides
    ExchangeRatesActivity providesActivity(){
        return activity;
    }

    @ActivityScope
    @Provides
    FragmentAdapterCurrency providesFragmentAdapterCurrency(ExchangeRatesActivity activity){
        return new FragmentAdapterCurrency(activity.getSupportFragmentManager(), activity.getIntent().getStringArrayListExtra(CURRENCY_LIST));
    }






}
