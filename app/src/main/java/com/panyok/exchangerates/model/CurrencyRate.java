package com.panyok.exchangerates.model;

import java.io.Serializable;

public class CurrencyRate implements Serializable {

    private String baseCurrency;

    private String currency;

    private float rate;

    public CurrencyRate(String baseCurrency, String currency, float rate) {
        this.baseCurrency = baseCurrency;
        this.currency = currency;
        this.rate = rate;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public float getRate() {
        return rate;
    }
}
