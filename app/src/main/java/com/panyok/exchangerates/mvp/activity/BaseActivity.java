package com.panyok.exchangerates.mvp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.io.BaseEncoding;
import com.panyok.exchangerates.ExchangeRatesApplication;
import com.panyok.exchangerates.R;
import com.panyok.exchangerates.dagger.component.AppComponent;
import com.panyok.exchangerates.dagger.component.BaseComponent;
import com.panyok.exchangerates.dagger.component.DaggerElement;
import com.panyok.exchangerates.enums.ErrorUI;
import com.panyok.exchangerates.mvp.presenter.BasePresenter;
import com.panyok.exchangerates.mvp.presenter.PresenterCache;
import com.panyok.exchangerates.mvp.view.BaseView;
import com.panyok.exchangerates.util.ExchangeRatesKeys;

import javax.inject.Inject;


public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements DaggerElement<BaseComponent>, ExchangeRatesKeys, BaseView {
    protected Toolbar mToolbar;


    private View mViewError;

    P mPresenter;

    @Inject
    PresenterCache mPresenterCache;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies(ExchangeRatesApplication.get(this).getAppComponent());
    }



    @SuppressWarnings("unchecked")
    protected void initPresenter(P presenter){
        if(presenter!=null) {
            mPresenter = mPresenterCache.getPresenter(getClass().getName());
            if (mPresenter == null) {
                mPresenter = presenter;
                mPresenterCache.putPresenter(getClass().getName(), mPresenter);
            }
            mPresenter.bindView(this);
        }
        Log.d("Lifecycle", "Create "+mPresenter.getView()+" "+mPresenter);
    }


    protected void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        initErrorView((ViewGroup) findViewById(R.id.root));
    }

    protected String chooseErrorMessage(ErrorUI errorUI) {
        switch (errorUI) {
            case ERR_UNEXPECTED:
                return getString(R.string.unexpected_error);

            case ERR_NO_NETWORK:
                return getString(R.string.network_error);

            case ERR_SERVER:
                return getString(R.string.server_error);

        }
        return "";
    }


    protected void toggleErrorView(ErrorUI errorUI) {
        if (mViewError != null) {
            if (errorUI != null) {
                String message = chooseErrorMessage(errorUI);
                ((TextView) mViewError.findViewById(R.id.tv_error)).setText(String.format(getString(R.string.base_error), message));
            }
            if (errorUI != null) {
                if (mViewError.getParent() == null) {
                    ((RelativeLayout) findViewById(R.id.root)).addView(mViewError, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                }
            } else {
                if (mViewError.getParent() != null)
                    ((RelativeLayout) findViewById(R.id.root)).removeView(mViewError);
            }
        }
    }


    private void initErrorView(ViewGroup view) {
        if (mViewError == null) {
            mViewError = LayoutInflater.from(this).inflate(R.layout.layout_error, view, false);
        }
        mViewError.setOnClickListener(view1 -> retry());

    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onStart() {
        super.onStart();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onRestart() {
        super.onRestart();
    }




    @Override
    protected void onStop() {
        super.onStop();

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if(mPresenter!=null) {
            if (!isChangingConfigurations()) {
                // activity is stopped normally, remove the cached presenter so it's not cached
                // even if activity gets killed
                mPresenterCache.removePresenter(mPresenter);
            }
            // onStop will clear view reference
            mPresenter.onStop(isChangingConfigurations());
        }
        super.onDestroy();
    }

    protected void retry() {

    }


    protected abstract void injectDependencies(AppComponent appComponent);
}
