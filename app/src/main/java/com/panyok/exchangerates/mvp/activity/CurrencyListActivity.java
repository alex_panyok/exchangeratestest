package com.panyok.exchangerates.mvp.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.panyok.exchangerates.R;
import com.panyok.exchangerates.dagger.component.AppComponent;
import com.panyok.exchangerates.dagger.component.BaseComponent;
import com.panyok.exchangerates.dagger.component.CurrencyListComponent;
import com.panyok.exchangerates.dagger.module.CurrencyListModule;
import com.panyok.exchangerates.enums.ErrorUI;
import com.panyok.exchangerates.mvp.adapter.CurrencyListAdapter;
import com.panyok.exchangerates.mvp.presenter.CurrencyListActivityPresenter;
import com.panyok.exchangerates.mvp.view.CurrencyListActivityView;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyListActivity extends BaseActivity<CurrencyListActivityPresenter> implements CurrencyListActivityView, SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, CurrencyListAdapter.OnSelectCurrencyListener {


    @BindView(R.id.rv_currency)
    RecyclerView mRecyclerViewCurrency;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Inject
    CurrencyListAdapter mAdapter;
    @Inject
    LinearLayoutManager mLinearLayoutManager;

    @Inject
    CurrencyListActivityPresenter presenter;


    private CurrencyListComponent mComponent;
    private MenuItem mSearchMenuItem;

    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initPresenter(presenter);
        ButterKnife.bind(this);
        initToolBar();
        initSwipeRefresh();
        initRv();
        checkInstanceState(savedInstanceState);
    }

    private void initSwipeRefresh() {
        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }


    private void checkInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null || !savedInstanceState.getBoolean(IS_LOADED) && !savedInstanceState.getBoolean(IS_REFRESHING))
            getData();
        else {
            mAdapter.setList(savedInstanceState.getStringArrayList(CURRENCY_LIST_DATA));
            mAdapter.filter(savedInstanceState.getString(SEARCH_QUERY));
        }
        if(savedInstanceState!=null)
            mSwipeRefreshLayout.setRefreshing(savedInstanceState.getBoolean(IS_REFRESHING));

    }


    private void initRv() {
        mRecyclerViewCurrency.setLayoutManager(mLinearLayoutManager);
        mRecyclerViewCurrency.setAdapter(mAdapter);
        mAdapter.setOnSelectCurrencyListener(this);
    }


    private void getData() {
        mSwipeRefreshLayout.setRefreshing(true);
        mPresenter.getCurrencyList();
        if (mSearchMenuItem != null)
            MenuItemCompat.collapseActionView(mSearchMenuItem);

    }

    @Override
    protected void injectDependencies(AppComponent appComponent) {
        mComponent = appComponent.plus(new CurrencyListModule(this));
        mComponent.inject(this);
    }

    @Override
    public BaseComponent getComponent() {
        return mComponent;
    }

    @Override
    public void setCurrencyList(List<String> list) {
        Log.d("Lifecycle", "loaded");
        toggleErrorView(null);
        mAdapter.setList(list);
        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CURRENCY_LIST_DATA, (Serializable) mAdapter.getItemsData());
        outState.putSerializable(IS_LOADED, !mAdapter.getItemsData().isEmpty());
        outState.putSerializable(SEARCH_QUERY, mAdapter.getSearchQuery());
        outState.putSerializable(IS_REFRESHING, mSwipeRefreshLayout.isRefreshing());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mSearchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) mSearchMenuItem.getActionView();
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        if (!mAdapter.getSearchQuery().isEmpty()) {
            MenuItemCompat.expandActionView(mSearchMenuItem);
            mSearchView.setQuery(mAdapter.getSearchQuery(), false);
        }
        mSearchView.setOnQueryTextListener(this);
        return true;

    }

    @Override
    public void errorLoadingCurrencyList(ErrorUI e) {
        mSwipeRefreshLayout.setRefreshing(false);
        toggleErrorView(e);
    }

    @Override
    public void onRefresh() {
        getData();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mAdapter.filter(query);
        mSearchView.clearFocus();
        return true;
    }

    @Override
    protected void retry() {
        getData();
    }

    @Override
    public boolean onQueryTextChange(String query) {
        mAdapter.filter(query);
        return true;
    }

    @Override
    public void select(List<String> currencies, String currency) {
        ExchangeRatesActivity.startExchangeRatesActivity(this, currencies, currency);
    }

}
