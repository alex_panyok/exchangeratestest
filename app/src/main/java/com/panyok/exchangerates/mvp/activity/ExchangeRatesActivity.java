package com.panyok.exchangerates.mvp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.panyok.exchangerates.R;
import com.panyok.exchangerates.dagger.component.AppComponent;
import com.panyok.exchangerates.dagger.component.BaseComponent;
import com.panyok.exchangerates.dagger.component.ExchangeRateComponent;
import com.panyok.exchangerates.dagger.module.ExchangeRateModule;
import com.panyok.exchangerates.mvp.adapter.FragmentAdapterCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ExchangeRatesActivity extends BaseActivity {

    private static final int VP_OFFSCREEN_LIMIT = 2;

    @Inject
    FragmentAdapterCurrency mAdapter;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    private ExchangeRateComponent component;

    public static void startExchangeRatesActivity(Context context, List<String> list, String currency) {
        Intent intent = new Intent(context, ExchangeRatesActivity.class);
        intent.putStringArrayListExtra(CURRENCY_LIST, new ArrayList<>(list));
        intent.putExtra(CURRENCY, currency);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rates);
        ButterKnife.bind(this);
        initToolBar();
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initViewPager();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void injectDependencies(AppComponent appComponent) {
        component = appComponent.plus(new ExchangeRateModule(this));
        component.inject(this);
    }

    private void initViewPager() {
        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(mAdapter.getItemByPosition(getIntent().getStringExtra(CURRENCY)), false);
    }

    @Override
    public BaseComponent getComponent() {
        return component;
    }
}
