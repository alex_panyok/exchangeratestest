package com.panyok.exchangerates.mvp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.panyok.exchangerates.mvp.adapter.holder.ItemCurrencyHolder;
import com.panyok.exchangerates.util.AdapterKeys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.Observable;


public class CurrencyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdapterKeys {

    private final Context mContext;

    private final List<String> itemsData = new ArrayList<>();

    private final List<String> items = new ArrayList<>();

    private Map<Integer, RVHolderFactory> viewHolderFactories;

    private OnSelectCurrencyListener onSelectCurrencyListener;



    private String searchQuery = "";

    public CurrencyListAdapter(Context context, Map<Integer, RVHolderFactory> viewHolderFactories) {
        this.mContext = context;
        this.viewHolderFactories = viewHolderFactories;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewHolderFactories.get(viewType).createViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(holder.getAdapterPosition()) == ITEM_CURRENCY) {
            ((ItemCurrencyHolder) holder).bind(items.get(holder.getAdapterPosition()));
            ((ItemCurrencyHolder) holder).itemView.setOnClickListener(view -> {
                if (onSelectCurrencyListener != null)
                    onSelectCurrencyListener.select(itemsData, items.get(holder.getAdapterPosition()));
            });
        }
    }


    public void filter(String query) {
        this.searchQuery = query;
        Observable.from(itemsData)
                .filter(s -> s.toLowerCase().contains(query))
                .toList()
                .subscribe(this::updateList);
    }


    @Override
    public int getItemViewType(int position) {
        return ITEM_CURRENCY;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateList(List<String> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void setList(List<String> items) {
        this.itemsData.clear();
        this.itemsData.addAll(items);
        updateList(itemsData);
    }

    public void setOnSelectCurrencyListener(OnSelectCurrencyListener listener) {
        this.onSelectCurrencyListener = listener;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public List<String> getItemsData(){
        return itemsData;
    }

    public List<String> getItems(){
        return items;
    }
    public interface OnSelectCurrencyListener {
        void select(List<String> currencies, String currency);
    }
}
