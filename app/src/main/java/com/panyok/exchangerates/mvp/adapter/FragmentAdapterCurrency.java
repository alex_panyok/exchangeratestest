package com.panyok.exchangerates.mvp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.panyok.exchangerates.mvp.fragment.ExchangeRateFragment;

import java.util.List;


public class FragmentAdapterCurrency extends FragmentStatePagerAdapter {


    private final List<String> currencyList;

    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public FragmentAdapterCurrency(FragmentManager fm, List<String> currencyList) {
        super(fm);
        this.currencyList = currencyList;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return currencyList.get(position);
    }


    public int getItemByPosition(String currency){
        return currencyList.indexOf(currency);
    }

    @Override
    public Fragment getItem(int position) {
        return ExchangeRateFragment.getInstance(new ExchangeRateFragment(), currencyList.get(position));
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }


    @Override
    public int getCount() {
        return currencyList.size();
    }
}
