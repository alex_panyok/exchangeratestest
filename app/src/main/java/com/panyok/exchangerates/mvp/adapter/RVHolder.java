package com.panyok.exchangerates.mvp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;


public abstract class RVHolder<T> extends RecyclerView.ViewHolder {
    public RVHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(T t);
}
