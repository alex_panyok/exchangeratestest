package com.panyok.exchangerates.mvp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;


public interface RVHolderFactory {
    RecyclerView.ViewHolder createViewHolder(ViewGroup parent);
}
