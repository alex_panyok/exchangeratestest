package com.panyok.exchangerates.mvp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.panyok.exchangerates.R;
import com.panyok.exchangerates.model.CurrencyRate;
import com.panyok.exchangerates.mvp.adapter.holder.ItemCurrencyHolder;
import com.panyok.exchangerates.util.AdapterKeys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RatesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdapterKeys {

    private final Context mContext;

    private final List<CurrencyRate> items = new ArrayList<>();

    private final Map<Integer, RVHolderFactory> viewHolderFactories;

    public RatesListAdapter(Context context, Map<Integer, RVHolderFactory> viewHolderFactories) {
        this.mContext = context;
        this.viewHolderFactories = viewHolderFactories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewHolderFactories.get(viewType).createViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(holder.getAdapterPosition()) == ITEM_CURRENCY) {
            CurrencyRate item = items.get(holder.getAdapterPosition());
            ((ItemCurrencyHolder) holder).bind(String.format(mContext.getString(R.string.rate_format), item.getBaseCurrency(), item.getCurrency(), item.getRate()));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM_CURRENCY;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateList(List<CurrencyRate> list){
        this.items.clear();
        this.items.addAll(list);
        notifyDataSetChanged();
    }

    public List<CurrencyRate> getItems(){
        return items;
    }
}
