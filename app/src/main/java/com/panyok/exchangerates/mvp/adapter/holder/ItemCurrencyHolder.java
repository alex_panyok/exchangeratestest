package com.panyok.exchangerates.mvp.adapter.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.auto.factory.AutoFactory;
import com.panyok.exchangerates.R;
import com.panyok.exchangerates.mvp.adapter.RVHolder;
import com.panyok.exchangerates.mvp.adapter.RVHolderFactory;

import butterknife.BindView;
import butterknife.ButterKnife;


@AutoFactory(implementing = RVHolderFactory.class)
public class ItemCurrencyHolder extends RVHolder<String> {

    @BindView(R.id.tv_currency)
    TextView mTextView;





    public ItemCurrencyHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_currency, parent, false));
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(String s) {
        mTextView.setText(s);
    }
}
