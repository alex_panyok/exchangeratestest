package com.panyok.exchangerates.mvp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.panyok.exchangerates.R;
import com.panyok.exchangerates.dagger.component.BaseComponent;
import com.panyok.exchangerates.dagger.component.DaggerElement;
import com.panyok.exchangerates.enums.ErrorUI;
import com.panyok.exchangerates.mvp.activity.BaseActivity;
import com.panyok.exchangerates.mvp.presenter.BasePresenter;
import com.panyok.exchangerates.mvp.presenter.PresenterCache;
import com.panyok.exchangerates.mvp.view.BaseView;
import com.panyok.exchangerates.util.ExchangeRatesKeys;

import javax.inject.Inject;


public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements ExchangeRatesKeys, BaseView {


    P mPresenter;
    private View mViewError;
    private ErrorUI mCurrentErrorUI;

    @Inject
    PresenterCache mPresenterCache;

    protected abstract void injectDependencies(BaseComponent activityComponent);


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectDependencies(getActComponent());
        initErrorView((ViewGroup) view.findViewById(R.id.root));
        checkInstanceSaved(savedInstanceState);
    }

    @SuppressWarnings("unchecked")
    protected void initPresenter(P presenter){
        if(presenter!=null) {
            mPresenter = mPresenterCache.getPresenter(getUniqueTag());
            if (mPresenter == null) {
                mPresenter = presenter;
                mPresenterCache.putPresenter(getUniqueTag(), mPresenter);
            }
            mPresenter.bindView(this);
        }
        Log.d("Lifecycle", "Create "+mPresenter);
    }

    private void checkInstanceSaved(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(IS_ERROR_SHOWN))
                toggleErrorView((ErrorUI) savedInstanceState.getSerializable(ERROR_UI));
        }
    }


    protected abstract String getUniqueTag();

    protected String chooseErrorMessage(ErrorUI errorUI) {
        if (errorUI != null) {
            switch (errorUI) {
                case ERR_UNEXPECTED:
                    return getString(R.string.unexpected_error);

                case ERR_NO_NETWORK:
                    return getString(R.string.network_error);

                case ERR_SERVER:
                    return getString(R.string.server_error);

            }
        }
        return "";
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mViewError != null && mViewError.getParent() != null && mCurrentErrorUI != null) {
            outState.putBoolean(IS_ERROR_SHOWN, true);
            outState.putSerializable(ERROR_UI, mCurrentErrorUI);
        }
    }

    protected void toggleErrorView(ErrorUI errorUI) {
        mCurrentErrorUI = errorUI;
        if (mViewError != null)
            ((TextView) mViewError.findViewById(R.id.tv_error)).setText(String.format(getString(R.string.base_error), chooseErrorMessage(errorUI)));
        if (errorUI != null) {
            if (getView() != null && mViewError != null && mViewError.getParent() == null)
                ((RelativeLayout) getView().findViewById(R.id.root)).addView(mViewError, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        } else {
            if (getView() != null && mViewError != null && mViewError.getParent() != null)
                ((RelativeLayout) getView().findViewById(R.id.root)).removeView(mViewError);
        }
    }


    private void initErrorView(ViewGroup view) {
        if (mViewError == null) {
            mViewError = LayoutInflater.from(getActivity()).inflate(R.layout.layout_error, view, false);
        }
        mViewError.setOnClickListener(view1 -> retry());

    }



    @SuppressWarnings("unchecked")
    @Override
    public void onStart() {
        super.onStart();
    }


//
//    @SuppressWarnings("unchecked")
//    @Override
//    public void onRestart() {
//        super.onRestart();
//        mPresenterCache.putPresenter(getClass().getName(), mPresenter);
//        mPresenter.bindView(this);
//    }
//
//
//
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (!getActivity().isChangingConfigurations()) {
//            // activity is stopped normally, remove the cached presenter so it's not cached
//            // even if activity gets killed
//            mPresenterCache.removePresenter(mPresenter);
//        }
//        // onStop will clear view reference
//        mPresenter.onStop(getActivity().isChangingConfigurations());
//        super.onStop();
//    }


    @Override
    public void onDestroyView() {
        if(mPresenter!=null) {
            if (!getActivity().isChangingConfigurations()) {
                // activity is stopped normally, remove the cached presenter so it's not cached
                // even if activity gets killed
                mPresenterCache.removePresenter(mPresenter);
            }
            // onStop will clear view reference
            mPresenter.onStop(getActivity().isChangingConfigurations());
        }
        super.onDestroyView();
    }


    protected abstract void retry();


    @SuppressWarnings("unchecked")
    public <T extends BaseComponent> T getActComponent() {
        BaseActivity activity = (BaseActivity) getActivity();
        DaggerElement<T> has = (DaggerElement<T>) activity;
        return has.getComponent();
    }
}
