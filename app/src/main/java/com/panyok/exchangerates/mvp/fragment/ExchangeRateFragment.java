package com.panyok.exchangerates.mvp.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Config;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.panyok.exchangerates.R;
import com.panyok.exchangerates.dagger.component.BaseComponent;
import com.panyok.exchangerates.dagger.component.ExchangeRateComponent;
import com.panyok.exchangerates.dagger.module.ExchangeRateFragmentModule;
import com.panyok.exchangerates.enums.ErrorUI;
import com.panyok.exchangerates.model.CurrencyRate;
import com.panyok.exchangerates.mvp.adapter.RatesListAdapter;
import com.panyok.exchangerates.mvp.presenter.ExchangeRateFragmentPresenter;
import com.panyok.exchangerates.mvp.view.ExchangeRateFragmentView;
import com.panyok.exchangerates.util.ExchangeRatesKeys;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExchangeRateFragment extends BaseFragment<ExchangeRateFragmentPresenter> implements ExchangeRateFragmentView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_currency)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    RatesListAdapter mAdapter;
    @Inject
    ExchangeRateFragmentPresenter presenter;

    public static ExchangeRateFragment getInstance(ExchangeRateFragment fragment, String currency) {
        Bundle bundle = new Bundle();
        bundle.putString(CURRENCY, currency);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rate, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initPresenter(presenter);
        initSwipeRefresh();
        initRv();
        checkSavedInstance(savedInstanceState);

    }

    @Override
    protected String getUniqueTag() {
        return getClass().getName()+getArguments().getString(CURRENCY);
    }

    private void initSwipeRefresh(){
        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @SuppressWarnings("unchecked")
    private void checkSavedInstance(Bundle savedInstanceState) {
        if(savedInstanceState==null || !savedInstanceState.getBoolean(IS_LOADED) && !mPresenter.isLoading()) {
            getData();
        }else {
            mAdapter.updateList((List<CurrencyRate>)savedInstanceState.getSerializable(CURRENCY_LIST));
        }
        if(savedInstanceState!=null)
            mSwipeRefreshLayout.setRefreshing(mPresenter.isLoading());
    }

    private void initRv() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getData(){
            mSwipeRefreshLayout.setRefreshing(true);
            mPresenter.getRates(getArguments().getString(CURRENCY));
        Log.d("Lifecycle2", mPresenter+" ");
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CURRENCY_LIST, (Serializable) mAdapter.getItems());
        outState.putSerializable(IS_LOADED, !mAdapter.getItems().isEmpty());
    }

    @Override
    protected void injectDependencies(BaseComponent activityComponent) {
        ((ExchangeRateComponent) activityComponent).plus(new ExchangeRateFragmentModule(this)).inject(this);
    }

    @Override
    protected void retry() {
        getData();
    }

    @Override
    public void setRatesList(List<CurrencyRate> list) {
        mSwipeRefreshLayout.setRefreshing(false);
        toggleErrorView(null);
        mAdapter.updateList(list);
    }

    @Override
    public void errorLoadingRatesList(ErrorUI errorUI) {
        mSwipeRefreshLayout.setRefreshing(false);
        toggleErrorView(errorUI);
    }


    @Override
    public void onDestroyView() {
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.removeAllViews();
        super.onDestroyView();

    }

    @Override
    public void onRefresh() {
        getData();
    }
}
