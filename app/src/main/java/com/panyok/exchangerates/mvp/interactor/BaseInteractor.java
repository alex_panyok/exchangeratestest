package com.panyok.exchangerates.mvp.interactor;

public interface BaseInteractor {

    void unsubscribe();
}
