package com.panyok.exchangerates.mvp.interactor;

import com.panyok.exchangerates.network.response.BaseResponse;

import rx.Observable;


public interface CurrencyListActivityInteractor extends BaseInteractor {

    Observable<BaseResponse> getCurrencyList();
}
