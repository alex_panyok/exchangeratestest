package com.panyok.exchangerates.mvp.interactor;

import com.panyok.exchangerates.network.response.BaseResponse;

import rx.Observable;


public interface ExchangeRateFragmentInteractor extends BaseInteractor {

    Observable<BaseResponse> getRatesList(String currency);
}
