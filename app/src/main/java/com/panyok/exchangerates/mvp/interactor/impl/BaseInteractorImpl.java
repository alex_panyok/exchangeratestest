package com.panyok.exchangerates.mvp.interactor.impl;


import com.panyok.exchangerates.mvp.interactor.BaseInteractor;
import com.panyok.exchangerates.network.ApiWorker;

import rx.subscriptions.CompositeSubscription;


public abstract class BaseInteractorImpl implements BaseInteractor {

    protected CompositeSubscription mSubscription;

    protected ApiWorker mApiWorker;


    public void unsubscribeSubscription() {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
            mSubscription = new CompositeSubscription();
        }
    }

    @Override
    public void unsubscribe() {
        unsubscribeSubscription();
    }


}
