package com.panyok.exchangerates.mvp.interactor.impl;

import com.panyok.exchangerates.mvp.interactor.CurrencyListActivityInteractor;
import com.panyok.exchangerates.mvp.listener.CurrencyListListener;
import com.panyok.exchangerates.network.ApiWorker;
import com.panyok.exchangerates.network.RetrofitException;
import com.panyok.exchangerates.network.response.BaseResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class CurrencyListActivityInteractorImpl extends BaseInteractorImpl implements CurrencyListActivityInteractor {


    public CurrencyListActivityInteractorImpl(ApiWorker mApiWorker, CompositeSubscription subscription) {
        this.mApiWorker = mApiWorker;
        this.mSubscription = subscription;
    }

    @Override
    public Observable<BaseResponse> getCurrencyList() {
        return mApiWorker.getCurrencyList()
                .delay(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }




}
