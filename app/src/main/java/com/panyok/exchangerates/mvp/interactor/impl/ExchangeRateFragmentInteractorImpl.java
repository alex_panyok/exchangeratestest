package com.panyok.exchangerates.mvp.interactor.impl;

import com.panyok.exchangerates.mvp.interactor.ExchangeRateFragmentInteractor;
import com.panyok.exchangerates.network.ApiWorker;
import com.panyok.exchangerates.network.response.BaseResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ExchangeRateFragmentInteractorImpl extends BaseInteractorImpl implements ExchangeRateFragmentInteractor {

    public ExchangeRateFragmentInteractorImpl(ApiWorker apiWorker, CompositeSubscription subscription) {
        this.mApiWorker = apiWorker;
        this.mSubscription = subscription;
    }

    @Override
    public Observable<BaseResponse> getRatesList(String currency) {
        return mApiWorker.getCurrencyList(currency)
                .delay(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
