package com.panyok.exchangerates.mvp.listener;


import com.panyok.exchangerates.network.response.BaseResponse;


public interface BaseResponseListener {

    void onSuccessResponse(BaseResponse r, String message);

}
