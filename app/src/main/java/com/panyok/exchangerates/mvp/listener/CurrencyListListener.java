package com.panyok.exchangerates.mvp.listener;

import com.panyok.exchangerates.network.RetrofitException;


public interface CurrencyListListener extends BaseResponseListener {

    void onErrorLoadingList(RetrofitException e);
}
