package com.panyok.exchangerates.mvp.listener;

import com.panyok.exchangerates.network.RetrofitException;

public interface RatesListListener extends BaseResponseListener {

    void onErrorLoadingRates(RetrofitException e);
}
