package com.panyok.exchangerates.mvp.presenter;

import com.panyok.exchangerates.mvp.view.BaseView;

public interface BasePresenter<T extends BaseView> {

    void unsubscribe();

    void bindView(T view);

    T getView();

    void unbindView();

    void onStart();

    void onStop(boolean isConfigurationChange);
}
