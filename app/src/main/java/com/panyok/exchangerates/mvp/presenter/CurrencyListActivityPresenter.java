package com.panyok.exchangerates.mvp.presenter;


import com.panyok.exchangerates.mvp.view.CurrencyListActivityView;

public interface CurrencyListActivityPresenter extends BasePresenter<CurrencyListActivityView> {

    void getCurrencyList();
}
