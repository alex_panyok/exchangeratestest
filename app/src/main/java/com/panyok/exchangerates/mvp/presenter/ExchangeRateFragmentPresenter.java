package com.panyok.exchangerates.mvp.presenter;

import com.panyok.exchangerates.mvp.view.ExchangeRateFragmentView;

public interface ExchangeRateFragmentPresenter extends BasePresenter<ExchangeRateFragmentView> {
    void getRates(String currency);

    boolean isLoading();
}
