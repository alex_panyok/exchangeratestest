package com.panyok.exchangerates.mvp.presenter.impl;

import android.util.Log;

import com.panyok.exchangerates.enums.ErrorUI;
import com.panyok.exchangerates.mvp.interactor.BaseInteractor;
import com.panyok.exchangerates.mvp.presenter.BasePresenter;
import com.panyok.exchangerates.mvp.view.BaseView;
import com.panyok.exchangerates.network.RetrofitException;

import java.lang.ref.WeakReference;


public abstract class BasePresenterImpl<I extends BaseInteractor, V extends BaseView> implements BasePresenter<V>{

    I mInteractor;

    protected WeakReference<V> viewReference;


    protected ErrorUI chooseErrorUIFromError(RetrofitException e){
        switch (e.getKind()){
            case HTTP:
                return ErrorUI.ERR_SERVER;
            case NETWORK:
                return ErrorUI.ERR_NO_NETWORK;
            case UNEXPECTED:
                return ErrorUI.ERR_UNEXPECTED;
            default:
                return ErrorUI.ERR_UNEXPECTED;
        }
    }


    @Override
    public void bindView(V view) {
        if (viewReference != null && viewReference.get() == view) {
            return;
        }
        this.viewReference = new WeakReference<>(view);
    }


    @Override
    public void unbindView() {
        this.viewReference = null;
    }





    @Override
    public void onStart() {
        Log.d("Lifecycle", "on_start");
    }


    @Override
    public V getView() {
        return viewReference.get();
    }

    @Override
    public void onStop(boolean isConfigurationChange) {
        Log.d("Lifecycle", "on_stop");
        if (!isConfigurationChange) {
            // clear all subscriptions if activity is stopped normally
            unsubscribe();
        }
        unbindView();
    }

    @Override
    public void unsubscribe() {
        mInteractor.unsubscribe();
    }
}
