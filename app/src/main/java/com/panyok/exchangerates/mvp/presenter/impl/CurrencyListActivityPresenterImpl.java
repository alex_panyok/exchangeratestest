package com.panyok.exchangerates.mvp.presenter.impl;


import android.util.Log;

import com.panyok.exchangerates.mvp.interactor.CurrencyListActivityInteractor;
import com.panyok.exchangerates.mvp.listener.CurrencyListListener;
import com.panyok.exchangerates.mvp.presenter.CurrencyListActivityPresenter;
import com.panyok.exchangerates.mvp.view.CurrencyListActivityView;
import com.panyok.exchangerates.network.RetrofitException;
import com.panyok.exchangerates.network.response.BaseResponse;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;


public class CurrencyListActivityPresenterImpl extends BasePresenterImpl<CurrencyListActivityInteractor, CurrencyListActivityView> implements CurrencyListActivityPresenter, CurrencyListListener {




    public CurrencyListActivityPresenterImpl(CurrencyListActivityInteractor mInteractor) {
        this.mInteractor = mInteractor;
    }


    @Override
    public void getCurrencyList() {
        mInteractor.getCurrencyList()
                    .subscribe(baseResponse -> {
                        viewReference.get().setCurrencyList(transformToStringList(baseResponse));
                    }, throwable -> {
                        viewReference.get().errorLoadingCurrencyList(chooseErrorUIFromError((RetrofitException)throwable));
                    });

    }

    @Override
    public void onErrorLoadingList(RetrofitException e) {
        Log.d("VIEW", viewReference+"");
        viewReference.get().errorLoadingCurrencyList(chooseErrorUIFromError(e));
    }

    @Override
    public void onSuccessResponse(BaseResponse r, String message) {
        Log.d("VIEW", viewReference+"");
        viewReference.get().setCurrencyList(transformToStringList(r));
    }


    private List<String> transformToStringList(BaseResponse response){
        List<String> currencyList = new ArrayList<>();
        currencyList.add(response.getBaseCurrency());
        currencyList.addAll(response.getCurrencyRates().keySet());
        return currencyList;
    }
}
