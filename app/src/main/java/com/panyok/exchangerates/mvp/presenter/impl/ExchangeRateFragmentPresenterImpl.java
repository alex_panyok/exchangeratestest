package com.panyok.exchangerates.mvp.presenter.impl;

import android.util.Log;

import com.panyok.exchangerates.model.CurrencyRate;
import com.panyok.exchangerates.mvp.interactor.ExchangeRateFragmentInteractor;
import com.panyok.exchangerates.mvp.listener.RatesListListener;
import com.panyok.exchangerates.mvp.presenter.ExchangeRateFragmentPresenter;
import com.panyok.exchangerates.mvp.view.ExchangeRateFragmentView;
import com.panyok.exchangerates.network.RetrofitException;
import com.panyok.exchangerates.network.response.BaseResponse;

import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.functions.Func1;

public class ExchangeRateFragmentPresenterImpl extends BasePresenterImpl<ExchangeRateFragmentInteractor, ExchangeRateFragmentView> implements ExchangeRateFragmentPresenter, RatesListListener {


    private Subscription mSubscription;

    private boolean isLoading;

    public ExchangeRateFragmentPresenterImpl(ExchangeRateFragmentInteractor mInteractor) {
        this.mInteractor = mInteractor;
    }

    @Override
    public void getRates(String currency) {
        isLoading = true;
        mSubscription = mInteractor.getRatesList(currency)
                .flatMap(new Func1<BaseResponse, Observable<Map.Entry<String, Float>>>() {
                    @Override
                    public Observable<Map.Entry<String, Float>> call(BaseResponse baseResponse) {
                        return Observable.from(baseResponse.getCurrencyRates().entrySet());
                    }
                })
                .map(stringFloatEntry -> new CurrencyRate(currency, stringFloatEntry.getKey(), stringFloatEntry.getValue()))
                .toList()
                .subscribe(list -> {
                    Log.d("BASE_CURRENCY", currency+"");
                    isLoading = false;
                    viewReference.get().setRatesList(list);
                }, throwable -> {
                    isLoading = false;
                    viewReference.get().errorLoadingRatesList(chooseErrorUIFromError(((RetrofitException) throwable)));
                });
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void onErrorLoadingRates(RetrofitException e) {
//        mView.errorLoadingRatesList(chooseErrorUIFromError(e));
    }

    @Override
    public void onSuccessResponse(BaseResponse r, String message) {
//        mSubscription = Observable.from(r.getCurrencyRates().entrySet())
//                .map(stringFloatEntry -> new CurrencyRate(r.getBaseCurrency(), stringFloatEntry.getKey(), stringFloatEntry.getValue()))
//                .toList()
//                .subscribe(currencyRates -> {
//                    mView.setRatesList(currencyRates);
//                }, throwable -> {
//                    mView.errorLoadingRatesList(ErrorUI.ERR_UNEXPECTED);
//                });
    }


    @Override
    public void unsubscribe() {
        super.unsubscribe();
        isLoading = false;
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
