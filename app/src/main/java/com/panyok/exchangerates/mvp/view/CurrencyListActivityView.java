package com.panyok.exchangerates.mvp.view;

import com.panyok.exchangerates.enums.ErrorUI;

import java.util.List;



public interface CurrencyListActivityView extends BaseView {

    void setCurrencyList(List<String> list);

    void errorLoadingCurrencyList(ErrorUI e);

}
