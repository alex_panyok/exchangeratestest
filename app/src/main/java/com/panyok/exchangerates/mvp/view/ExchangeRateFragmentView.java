package com.panyok.exchangerates.mvp.view;

import com.panyok.exchangerates.enums.ErrorUI;
import com.panyok.exchangerates.model.CurrencyRate;

import java.util.List;

public interface ExchangeRateFragmentView extends BaseView {

    void setRatesList(List<CurrencyRate> list);

    void errorLoadingRatesList(ErrorUI errorUI);

}
