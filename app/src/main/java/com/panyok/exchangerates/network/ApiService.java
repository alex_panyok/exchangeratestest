package com.panyok.exchangerates.network;


import com.panyok.exchangerates.network.response.BaseResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface ApiService {

    @GET("/latest")
    Observable<BaseResponse> getCurrencyList();

    @GET("/latest")
    Observable<BaseResponse> getCurrencyListWithBase(@Query("base") String currency);

}
