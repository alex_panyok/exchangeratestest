package com.panyok.exchangerates.network;

import com.panyok.exchangerates.network.keys.ApiKeys;
import com.panyok.exchangerates.network.response.BaseResponse;

import rx.Observable;


public class ApiWorker implements ApiKeys {

    private ApiService apiService;

    public ApiWorker(ApiService apiService) {
        this.apiService = apiService;
    }


    public Observable<BaseResponse> getCurrencyList(){
        return apiService.getCurrencyList();
    }


    public Observable<BaseResponse> getCurrencyList(String currency){
        return apiService.getCurrencyListWithBase(currency);
    }


}
