package com.panyok.exchangerates.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.Map;


public class BaseResponse  {



    @SerializedName("base")
    private String baseCurrency;


    @SerializedName("rates")
    private Map<String, Float> currencyRates;


    public BaseResponse(String baseCurrency, Map<String, Float> currencyRates) {
        this.baseCurrency = baseCurrency;
        this.currencyRates = currencyRates;
    }



    public String getBaseCurrency() {
        return baseCurrency;
    }

    public Map<String, Float> getCurrencyRates() {
        return currencyRates;
    }

}
