package com.panyok.exchangerates.util;



public interface ExchangeRatesKeys {
    String CURRENCY = "currency";

    String CURRENCY_LIST = "currency_list";

    String CURRENCY_LIST_DATA = "currency_list_data";

    String IS_ERROR_SHOWN = "is_error_shown";

    String IS_LOADED = "is_loaded";

    String ERROR_UI = "error_ui";

    String SEARCH_QUERY = "search_query";

    String IS_REFRESHING = "is_refreshing";

    String ACT_SUBSCRIPTION = "act_subs";

    String FRAG_SUBSCRIPTION = "frag_subs";
}
