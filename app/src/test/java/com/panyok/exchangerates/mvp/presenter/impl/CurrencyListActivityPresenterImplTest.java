package com.panyok.exchangerates.mvp.presenter.impl;

import com.panyok.exchangerates.mvp.interactor.CurrencyListActivityInteractor;
import com.panyok.exchangerates.mvp.listener.CurrencyListListener;
import com.panyok.exchangerates.mvp.view.CurrencyListActivityView;
import com.panyok.exchangerates.network.response.BaseResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;

public class CurrencyListActivityPresenterImplTest {


    @Mock
    CurrencyListActivityView view;
    @Mock
    CurrencyListActivityInteractor interactor;
    @Mock
    private BaseResponse baseResponse;
    @Captor
    private ArgumentCaptor<CurrencyListListener> mCurrencyListListener;

    private CurrencyListActivityPresenterImpl presenter;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new CurrencyListActivityPresenterImpl(view, interactor);

    }

    @Test
    public void getCurrencyList() throws Exception {
        presenter.getCurrencyList();
        verify(interactor).getCurrencyList(mCurrencyListListener.capture());
        mCurrencyListListener.getValue().onSuccessResponse(baseResponse, null);
        verify(view).setCurrencyList(anyList());
    }

}