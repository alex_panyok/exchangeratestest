package com.panyok.exchangerates.mvp.presenter.impl;

import com.panyok.exchangerates.model.CurrencyRate;
import com.panyok.exchangerates.mvp.interactor.ExchangeRateFragmentInteractor;
import com.panyok.exchangerates.mvp.listener.RatesListListener;
import com.panyok.exchangerates.mvp.view.ExchangeRateFragmentView;
import com.panyok.exchangerates.network.response.BaseResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class ExchangeRateFragmentPresenterTest {


    private static String BASE_CURRENCY = "USD";


    @Mock
    BaseResponse response;


    @Mock
    List<CurrencyRate> list;


    @Mock
    private ExchangeRateFragmentView view;


    @Mock
    private ExchangeRateFragmentInteractor interactor;

    @Captor
    private ArgumentCaptor<RatesListListener> listener;

    private ExchangeRateFragmentPresenterImpl presenter;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new ExchangeRateFragmentPresenterImpl(view, interactor);
    }


    @Test
    public void getRates() throws Exception {
        presenter.getRates(BASE_CURRENCY);
        verify(interactor).getRatesList(eq(BASE_CURRENCY), listener.capture());
        listener.getValue().onSuccessResponse(response, null);
        verify(view).setRatesList(anyList());
    }

}